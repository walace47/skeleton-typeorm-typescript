import * as jwt from 'jsonwebtoken';
import { Request, Response,NextFunction } from "express";



export const auth = (req: Request, res: Response, next: NextFunction)=>{
    try{
        const token = <string>req.headers["auth"];
        console.log(token)
        let jwtPayload =(<any>jwt.verify(token, process.env.SEED)).usuario;
        res.locals.jwtPayload = jwtPayload;

        const { userId, username } = jwtPayload;
        const newToken = jwt.sign({ userId, username }, process.env.SEED, {
          expiresIn: "2h"
        });

        res.setHeader("token", newToken);
        next();
    }catch(err){
        return res.status(401).json({
            error:err,
            ok:false
        })
    }
}


