import { Router } from "express";
import { userController } from "../controllers";

const router = Router();

router.get("/",userController.getUsers);
router.get("/:id", userController.getUser);
router.post("/",userController.createUser);
//router.put("/",)

export = router;