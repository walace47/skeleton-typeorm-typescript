import {Router} from "express";
import * as userRoute from "./user.routes"
const router = Router();

router.use('/user',userRoute);

export = router;