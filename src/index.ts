import * as env from 'dotenv';
env.config();

import "reflect-metadata";
import { createConnection,ConnectionOptions } from "typeorm";
import Server from "./server/Server";
import * as routes from './routes';
import { install } from 'source-map-support';
import { configDatabase} from './config';


createConnection(configDatabase)
	.then(con => {
		install();

		const server = Server.init(Number(process.env.PORT));

		//Rutas Api
		server.app.use('/api', routes);


		server.start(() => {
			console.log(`servidor corriendo en el puerto http://localhost:${process.env.PORT}/api`);
		});
	})
	.catch(error =>
		console.log({ error, mg: "No se pudo conectar a la base de datos" })
	);
