import * as express from "express";
import * as server from 'http';
import * as path from 'path';
import * as cors from 'cors';


export default class Server {
  
  public app: express.Application;
  public port: number;
  public server: any;

  constructor(port: number) {
    this.port = port;
    this.app = express();

    this.server = server.createServer(this.app);
    this.server.use(cors());
    this.server.use(express.json({ limit: "50mb" }))
    this.server.use(express.urlencoded({ extended: true, limit: "50mb" }));
    //Carpeta donde se almacenaran archivos de la aplicacion
    this.app.use("/api/files", express.static(path.join(__dirname, '../../public')));
  }

  static init(port: number) {
    return new Server(port);
  }

  start(callback: () => void) {
    this.server.listen(this.port, callback);
  }
}
